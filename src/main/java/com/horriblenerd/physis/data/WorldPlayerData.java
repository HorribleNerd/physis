package com.horriblenerd.physis.data;

import com.horriblenerd.physis.PhysisConstants;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.storage.WorldSavedData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.UUID;

import static com.horriblenerd.physis.PhysisConstants.MODID;

/**
 * Created by HorribleNerd on 04/12/2020
 */
public class WorldPlayerData extends WorldSavedData {
    private static final String DATA_NAME = MODID + "_player_data";

    private static final Logger LOGGER = LogManager.getLogger();

    private HashMap<UUID, Integer> STORAGE = new HashMap<>();

    public static final WorldPlayerData PLAYER_DATA = new WorldPlayerData();

    public WorldPlayerData() {
        super(DATA_NAME);
    }

    public WorldPlayerData(String s) {
        super(s);
    }

    public static WorldPlayerData get(ServerWorld world) {
        WorldPlayerData instance = world.getSavedData().getOrCreate(WorldPlayerData::new, DATA_NAME);
        world.getSavedData().set(instance);
        return instance;
    }

    public void addPlayerStanding(UUID uuid, int standing) {
        STORAGE.put(uuid, getPlayerStanding(uuid) + standing);
        markDirty();
    }

    public void subtractPlayerStanding(UUID uuid, int standing) {
        STORAGE.put(uuid, getPlayerStanding(uuid) - standing);
        markDirty();
    }

    public void setPlayerStanding(UUID uuid, int standing) {
        STORAGE.put(uuid, standing);
        markDirty();
    }

    public int getPlayerStanding(UUID uuid) {
        if (STORAGE.containsKey(uuid)) {
            return STORAGE.get(uuid);
        }
        STORAGE.put(uuid, PhysisConstants.STARTING_STANDING);
        return PhysisConstants.STARTING_STANDING;
    }

    public int getPlayerStanding(String uuid) {
        return getPlayerStanding(UUID.fromString(uuid));
    }

    @Override
    public void read(final CompoundNBT nbt) {
        for (String key : nbt.keySet()) {
            UUID uuid = UUID.fromString(key);
            STORAGE.put(uuid, nbt.getInt(key));
        }
    }

    @Override
    public CompoundNBT write(final CompoundNBT nbt) {
        STORAGE.forEach((uuid, i) -> nbt.putInt(uuid.toString(), i));
        return nbt;
    }

}
