package com.horriblenerd.physis.capability.necklace;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;

/**
 * Created by HorribleNerd on 11/12/2020
 */
public class CapabilityNecklace {
    @CapabilityInject(INecklaceStorage.class)
    public static Capability<INecklaceStorage> NECKLACE = null;

    public static void register() {
        CapabilityManager.INSTANCE.register(INecklaceStorage.class, new Capability.IStorage<INecklaceStorage>() {
            @Override
            public INBT writeNBT(Capability<INecklaceStorage> capability, INecklaceStorage instance, Direction side) {
                return instance.serializeNBT();
            }

            @Override
            public void readNBT(Capability<INecklaceStorage> capability, INecklaceStorage instance, Direction side, INBT nbt) {
                if (!(instance instanceof NecklaceStorage)) {
                    throw new IllegalArgumentException("Can not deserialize to an instance that isn't the default implementation");
                }
                instance.deserializeNBT((CompoundNBT) nbt);
            }
        },NecklaceStorage::new);
    }

}
