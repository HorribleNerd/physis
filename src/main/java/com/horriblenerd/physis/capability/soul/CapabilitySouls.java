package com.horriblenerd.physis.capability.soul;

import net.minecraft.nbt.INBT;
import net.minecraft.nbt.IntNBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;

import static com.horriblenerd.physis.PhysisConstants.STARTING_SOULS;

/**
 * Created by HorribleNerd on 04/12/2020
 */

public class CapabilitySouls {
    @CapabilityInject(ISoulStorage.class)
    public static Capability<ISoulStorage> SOULS = null;

    public static void register() {
        CapabilityManager.INSTANCE.register(ISoulStorage.class, new IStorage<ISoulStorage>() {
                    @Override
                    public INBT writeNBT(Capability<ISoulStorage> capability, ISoulStorage instance, Direction side) {
                        return IntNBT.valueOf(instance.getSoulsStored());
                    }

                    @Override
                    public void readNBT(Capability<ISoulStorage> capability, ISoulStorage instance, Direction side, INBT nbt) {
                        if (!(instance instanceof SoulStorage)) {
                            throw new IllegalArgumentException("Can not deserialize to an instance that isn't the default implementation");
                        }
                        ((SoulStorage) instance).souls = ((IntNBT) nbt).getInt();
                    }
                },
                () -> new SoulStorage(STARTING_SOULS));
    }
}