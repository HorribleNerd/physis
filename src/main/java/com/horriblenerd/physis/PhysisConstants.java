package com.horriblenerd.physis;

/**
 * Created by HorribleNerd on 04/12/2020
 */
public class PhysisConstants {

    public static final String MODID = "physis";

    public static final int STARTING_SOULS = 1000;
    public static final int STARTING_STANDING = 100;

    public static final float LIFESTEAL_MODIFIER_PLAYER = 0.2f;
    public static final float LIFESTEAL_MODIFIER_MOB = 0.2f;

    public static int getSoulCollectorAmount(int standing) {
        return Math.max(standing, 0) * 1 + (standing > 0 ? 10 : 0);
    }

}
