package com.horriblenerd.physis.capability.owner;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.server.ServerWorld;

import java.util.UUID;

/**
 * Created by HorribleNerd on 04/12/2020
 */
public interface IOwnerStorage {

    UUID getOwnerUUID();

    String getOwnerName();

    void setOwnerUUID(UUID uuid);

    void setOwnerName(String name);

    int getOwnerStanding(ServerWorld serverWorld);

    void deserializeNBT(CompoundNBT tag);

    CompoundNBT serializeNBT();

}
