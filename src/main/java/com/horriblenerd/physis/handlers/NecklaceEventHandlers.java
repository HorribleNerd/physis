package com.horriblenerd.physis.handlers;

import com.horriblenerd.physis.PhysisConstants;
import com.horriblenerd.physis.capability.necklace.CapabilityNecklace;
import com.horriblenerd.physis.capability.necklace.INecklaceStorage;
import com.horriblenerd.physis.capability.necklace.NecklacePovider;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.BlastingRecipe;
import net.minecraft.item.crafting.FurnaceRecipe;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.item.crafting.RecipeManager;
import net.minecraft.loot.LootContext;
import net.minecraft.loot.LootParameters;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.IWorld;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingKnockBackEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;

import static com.horriblenerd.physis.PhysisConstants.MODID;

/**
 * Created by HorribleNerd on 10/12/2020
 */
public class NecklaceEventHandlers {

    private static final Logger LOGGER = LogManager.getLogger();

    @SubscribeEvent
    public static void attachCapabilities(AttachCapabilitiesEvent<Entity> event) {
        if (event.getObject() instanceof PlayerEntity) {
            NecklacePovider provider = new NecklacePovider();
            event.addCapability(new ResourceLocation(MODID, "necklace"), provider);
        }
    }

    @SubscribeEvent
    public static void onBlockBreak(BlockEvent.BreakEvent event) {
        IWorld world = event.getWorld();
        if (world instanceof ServerWorld) {
            ServerWorld serverWorld = (ServerWorld) world;
            PlayerEntity player = event.getPlayer();
            LazyOptional<INecklaceStorage> cap = player.getCapability(CapabilityNecklace.NECKLACE);
            cap.ifPresent(necklaceStorage -> {
                if (necklaceStorage.getTier(INecklaceStorage.Blessing.UTILITY) >= 1) {
                    BlockState state = event.getState();

                    List<ItemStack> drops = state.getDrops(new LootContext.Builder(serverWorld).withParameter(LootParameters.TOOL, player.getHeldItemMainhand()).withNullableParameter(LootParameters.THIS_ENTITY, player).withParameter(LootParameters.field_237457_g_, Vector3d.copyCentered(event.getPos())));
                    RecipeManager recipeManager = serverWorld.getRecipeManager();
                    for (ItemStack stack : drops) {

                        ItemStack newStack;
                        float exp;

                        // TODO: black/whitelist
                        LOGGER.debug(stack);
                        Optional<FurnaceRecipe> smeltingRecipe = recipeManager.getRecipe(IRecipeType.SMELTING, new Inventory(stack), serverWorld);

                        if (smeltingRecipe.isPresent()) {
                            LOGGER.debug("Drops: smelting");
                            FurnaceRecipe recipe = smeltingRecipe.get();
                            newStack = recipe.getRecipeOutput().copy();
                            LOGGER.debug(newStack);
                            exp = recipe.getExperience();
                            if (!newStack.isEmpty() && !player.addItemStackToInventory(newStack)) {
                                player.entityDropItem(newStack);
                            }
                            player.giveExperiencePoints((int) exp);
                        }
                        else {
                            Optional<BlastingRecipe> blastingRecipe = recipeManager.getRecipe(IRecipeType.BLASTING, new Inventory(stack), serverWorld);
                            if (blastingRecipe.isPresent()) {
                                LOGGER.debug("Drops: blasting");
                                BlastingRecipe recipe = blastingRecipe.get();
                                newStack = recipe.getRecipeOutput().copy();
                                LOGGER.debug(newStack);
                                exp = recipe.getExperience();
                                if (!newStack.isEmpty() && !player.addItemStackToInventory(newStack)) {
                                    player.entityDropItem(newStack);
                                }
                                player.giveExperiencePoints((int) exp);
                            }
                        }


                    }
                }
            });
        }
    }


    @SubscribeEvent
    public static void onKnockback(LivingKnockBackEvent event) {
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) event.getEntityLiving();
            LazyOptional<INecklaceStorage> cap = player.getCapability(CapabilityNecklace.NECKLACE);
            cap.ifPresent((necklaceStorage) -> {
                if (necklaceStorage.getTier(INecklaceStorage.Blessing.ATTACK) >= 1) {
                    event.setStrength(event.getStrength() * 1.5f);
                }
            });
        }
    }

    @SubscribeEvent
    public static void onAttack(LivingAttackEvent event) {
        Entity trueSource = event.getSource().getTrueSource();
        if (trueSource instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) trueSource;
            LazyOptional<INecklaceStorage> cap = player.getCapability(CapabilityNecklace.NECKLACE);
            cap.ifPresent((necklaceStorage) -> {
                if (necklaceStorage.getTier(INecklaceStorage.Blessing.ATTACK) >= 1) {
                    player.addPotionEffect(new EffectInstance(Effects.SPEED, 4 * 20, 2));
                }
            });
        }
    }

    @SubscribeEvent
    public static void onDamage(LivingDamageEvent event) {
        Entity trueSource = event.getSource().getTrueSource();
        // Player dealing damage
        if (trueSource instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) trueSource;
            LazyOptional<INecklaceStorage> cap = player.getCapability(CapabilityNecklace.NECKLACE);
            cap.ifPresent((necklaceStorage) -> {
                necklaceStorage.setTier(INecklaceStorage.Blessing.ATTACK, 1);
                if (necklaceStorage.getTier(INecklaceStorage.Blessing.ATTACK) >= 1) {
                    player.heal(PhysisConstants.LIFESTEAL_MODIFIER_PLAYER * (event.getAmount()));
                }
            });
        }
        // Player getting hit
        // No else because of possible pvp
        // However, this will mean it will trigger twice in that case, thus doubling healing if both players have the blessing
        // TODO: possibly remove?
        if (event.getEntityLiving() instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) event.getEntityLiving();
            LazyOptional<INecklaceStorage> cap = player.getCapability(CapabilityNecklace.NECKLACE);
            cap.ifPresent((necklaceStorage) -> {
                if (necklaceStorage.getTier(INecklaceStorage.Blessing.ATTACK) >= 0) {
                    event.getEntityLiving().heal(PhysisConstants.LIFESTEAL_MODIFIER_MOB * (event.getAmount()));
                }
            });
        }
    }

}
