package com.horriblenerd.physis.setup;

import com.horriblenerd.physis.gui.NecklaceButton;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

import static com.horriblenerd.physis.PhysisConstants.MODID;

/**
 * Created by HorribleNerd on 10/12/2020
 */
@Mod.EventBusSubscriber(modid = MODID, value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ClientSetup {

    public static void init(final FMLClientSetupEvent event) {
        MinecraftForge.EVENT_BUS.addListener(NecklaceButton::addInventoryButton);
    }

}
