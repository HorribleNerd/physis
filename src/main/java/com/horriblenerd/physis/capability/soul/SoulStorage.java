package com.horriblenerd.physis.capability.soul;

import net.minecraft.nbt.CompoundNBT;

/**
 * Created by HorribleNerd on 04/12/2020
 */
public class SoulStorage implements ISoulStorage {

    protected int souls;
    protected int capacity;
    protected int maxReceive;
    protected int maxExtract;

    public SoulStorage(int capacity) {
        this(capacity, capacity, capacity, 0);
    }

    public SoulStorage(int capacity, int maxTransfer) {
        this(capacity, maxTransfer, maxTransfer, 0);
    }

    public SoulStorage(int capacity, int maxReceive, int maxExtract) {
        this(capacity, maxReceive, maxExtract, 0);
    }

    public SoulStorage(int capacity, int maxReceive, int maxExtract, int souls) {
        this.capacity = capacity;
        this.maxReceive = maxReceive;
        this.maxExtract = maxExtract;
        this.souls = Math.max(0, Math.min(capacity, souls));
    }

    @Override
    public int receiveSouls(int maxReceive, boolean simulate) {
        if (!canReceive()) {
            return 0;
        }

        int soulsReceived = Math.min(capacity - souls, Math.min(this.maxReceive, maxReceive));
        if (!simulate) {
            souls += soulsReceived;
        }
        return soulsReceived;
    }

    public int extractSouls(int maxExtract, boolean simulate) {
        if (!canExtract()) {
            return 0;
        }

        int soulsExtracted = Math.min(souls, Math.min(this.maxExtract, maxExtract));
        if (!simulate) {
            souls -= soulsExtracted;
        }
        return soulsExtracted;
    }

    public void setSouls(int souls) {
        this.souls = souls;
        onSoulsChanged();
    }

    public void addSouls(int souls) {
        this.souls += souls;
        if (this.souls > getMaxSoulsStored()) {
            this.souls = getMaxSoulsStored();
        }
        onSoulsChanged();
    }

    public void removeSouls(int souls) {
        this.souls -= souls;
        if (this.souls < 0) {
            this.souls = 0;
        }
        onSoulsChanged();
    }

    @Override
    public void onSoulsChanged() {

    }

    @Override
    public int getSoulsStored() {
        return souls;
    }

    @Override
    public int getMaxSoulsStored() {
        return capacity;
    }

    @Override
    public int getMaxReceive() {
        return maxReceive;
    }

    @Override
    public int getMaxExtract() {
        return maxExtract;
    }

    @Override
    public boolean canExtract() {
        return this.maxExtract > 0;
    }

    @Override
    public boolean canReceive() {
        return this.maxReceive > 0;
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.putInt("souls", getSoulsStored());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        setSouls(nbt.getInt("souls"));
    }

}
