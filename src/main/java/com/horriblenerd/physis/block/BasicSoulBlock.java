package com.horriblenerd.physis.block;

import com.horriblenerd.physis.block.tile.BasicSoulTile;
import com.horriblenerd.physis.capability.owner.CapabilityOwner;
import com.horriblenerd.physis.util.WorldUtil;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

import javax.annotation.Nullable;

/**
 * Created by HorribleNerd on 04/12/2020
 */
public class BasicSoulBlock extends Block {

    public BasicSoulBlock(Properties properties) {
        super(properties);
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack stack) {
        BasicSoulTile tile = WorldUtil.getTileEntity(BasicSoulTile.class, worldIn, pos);
        if (tile != null) {
            if (!worldIn.isRemote()) {
                PlayerEntity player = (PlayerEntity) placer;
                tile.getCapability(CapabilityOwner.OWNER).ifPresent(ownerStorage -> {
                    ownerStorage.setOwnerUUID(player.getUniqueID());
                    ownerStorage.setOwnerName(player.getName().getString());
                });


//                    WorldPlayerData.get((ServerWorld) worldIn).subtractPlayerStanding(player.getUniqueID(), 10);
            }
        }
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return true;
    }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return new BasicSoulTile();
    }
}
