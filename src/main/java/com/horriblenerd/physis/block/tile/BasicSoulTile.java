package com.horriblenerd.physis.block.tile;

import com.horriblenerd.physis.capability.owner.CapabilityOwner;
import com.horriblenerd.physis.capability.owner.IOwnerStorage;
import com.horriblenerd.physis.capability.owner.OwnerStorage;
import com.horriblenerd.physis.capability.soul.CapabilitySouls;
import com.horriblenerd.physis.capability.soul.ISoulStorage;
import com.horriblenerd.physis.capability.soul.SoulStorage;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import java.util.UUID;

import static com.horriblenerd.physis.setup.Registration.BASIC_SOUL_TILE;

/**
 * Created by HorribleNerd on 04/12/2020
 */
public class BasicSoulTile extends TileEntity implements ITickableTileEntity {

    private static final Logger LOGGER = LogManager.getLogger();

    private SoulStorage soulStorage;
    private LazyOptional<ISoulStorage> souls;

    private OwnerStorage ownerStorage;
    private LazyOptional<IOwnerStorage> owner;

    public BasicSoulTile() {
        super(BASIC_SOUL_TILE.get());
        soulStorage = createSoulStorage(1000, 1000, 1000, 1);
        souls = LazyOptional.of(() -> soulStorage);

        ownerStorage = createOwnerStorage();
        owner = LazyOptional.of(() -> ownerStorage);
    }

    @Override
    public void tick() {
        if (world.isRemote) {
            return;
        }

//        getCapability(CapabilityOwner.OWNER).ifPresent((ownerStorage1 -> LOGGER.debug(ownerStorage1.getOwnerName())));
//        LOGGER.debug(ownerStorage.getOwnerName());
//        LOGGER.debug(ownerStorage.getOwnerUUID());
//        LOGGER.debug(soulStorage.getSoulsStored());
    }

    @Override
    public void remove() {
        souls.invalidate();
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if (cap == CapabilitySouls.SOULS) {
            return souls.cast();
        }
        if (cap == CapabilityOwner.OWNER) {
            return owner.cast();
        }
        return super.getCapability(cap, side);
    }

    private SoulStorage createSoulStorage(int capacity, int maxReceive, int maxExtract, int souls) {
        return new SoulStorage(capacity, maxReceive, maxExtract, souls) {
            @Override
            public void onSoulsChanged() {
                markDirty();
            }
        };
    }

    private OwnerStorage createOwnerStorage() {
        return new OwnerStorage();
    }

    @Override
    public void read(BlockState state, CompoundNBT tag) {
        super.read(state, tag);
        soulStorage.deserializeNBT(tag.getCompound("souls"));
        ownerStorage.deserializeNBT(tag.getCompound("owner"));
    }

    @Override
    public CompoundNBT write(CompoundNBT tag) {
        super.write(tag);
        tag.put("souls", soulStorage.serializeNBT());
        tag.put("owner", ownerStorage.serializeNBT());
        return tag;
    }

}
