package com.horriblenerd.physis.capability.owner;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;

/**
 * Created by HorribleNerd on 04/12/2020
 */
public class CapabilityOwner {
    @CapabilityInject(IOwnerStorage.class)
    public static Capability<IOwnerStorage> OWNER = null;

    public static void register() {
        CapabilityManager.INSTANCE.register(IOwnerStorage.class, new Capability.IStorage<IOwnerStorage>() {
                    @Override
                    public INBT writeNBT(Capability<IOwnerStorage> capability, IOwnerStorage instance, Direction side) {
                        return instance.serializeNBT();
                    }

                    @Override
                    public void readNBT(Capability<IOwnerStorage> capability, IOwnerStorage instance, Direction side, INBT nbt) {
                        if (!(instance instanceof OwnerStorage)) {
                            throw new IllegalArgumentException("Can not deserialize to an instance that isn't the default implementation");
                        }
                        instance.deserializeNBT((CompoundNBT) nbt);
                    }
                },
                OwnerStorage::new);
    }
}

