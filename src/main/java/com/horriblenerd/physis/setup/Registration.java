package com.horriblenerd.physis.setup;

import com.horriblenerd.physis.block.BasicSoulBlock;
import com.horriblenerd.physis.block.SoulCollectorBlock;
import com.horriblenerd.physis.block.tile.BasicSoulTile;
import com.horriblenerd.physis.block.tile.SoulCollectorTile;
import com.horriblenerd.physis.item.DebugItem;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.entity.EntityType;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

import static com.horriblenerd.physis.PhysisConstants.MODID;
import static com.horriblenerd.physis.setup.ModSetup.ITEM_GROUP;

/**
 * Created by HorribleNerd on 04/12/2020
 */
public class Registration {

    private static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, MODID);
    private static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, MODID);
    private static final DeferredRegister<TileEntityType<?>> TILES = DeferredRegister.create(ForgeRegistries.TILE_ENTITIES, MODID);
    private static final DeferredRegister<ContainerType<?>> CONTAINERS = DeferredRegister.create(ForgeRegistries.CONTAINERS, MODID);
    private static final DeferredRegister<EntityType<?>> ENTITIES = DeferredRegister.create(ForgeRegistries.ENTITIES, MODID);


    public static void init() {
        BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
        ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
        TILES.register(FMLJavaModLoadingContext.get().getModEventBus());
        CONTAINERS.register(FMLJavaModLoadingContext.get().getModEventBus());
        ENTITIES.register(FMLJavaModLoadingContext.get().getModEventBus());
    }

    public static final RegistryObject<Item> DEBUG_ITEM = ITEMS.register("debug_item", () -> new DebugItem(ArmorMaterial.LEATHER, new Item.Properties().group(ITEM_GROUP)));

    public static final RegistryObject<SoulCollectorBlock> SOUL_COLLECTOR_BLOCK = BLOCKS.register("soul_collector", () -> new SoulCollectorBlock(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.STONE)));
    public static final RegistryObject<Item> SOUL_COLLECTOR_ITEM = ITEMS.register("soul_collector", () -> new BlockItem(SOUL_COLLECTOR_BLOCK.get(), new Item.Properties().group(ITEM_GROUP)));
    public static final RegistryObject<TileEntityType<SoulCollectorTile>> SOUL_COLLECTOR_TILE = TILES.register("soul_collector", () -> TileEntityType.Builder.create(SoulCollectorTile::new, SOUL_COLLECTOR_BLOCK.get()).build(null));

    public static final RegistryObject<BasicSoulBlock> BASIC_SOUL_BLOCK = BLOCKS.register("basic_soul", () -> new BasicSoulBlock(AbstractBlock.Properties.create(Material.ROCK, MaterialColor.STONE)));
    public static final RegistryObject<Item> BASIC_SOUL_ITEM = ITEMS.register("basic_soul", () -> new BlockItem(BASIC_SOUL_BLOCK.get(), new Item.Properties().group(ITEM_GROUP)));
    public static final RegistryObject<TileEntityType<BasicSoulTile>> BASIC_SOUL_TILE = TILES.register("basic_soul", () -> TileEntityType.Builder.create(BasicSoulTile::new, BASIC_SOUL_BLOCK.get()).build(null));

}
