package com.horriblenerd.physis.capability.necklace;

import net.minecraft.nbt.CompoundNBT;

import java.util.HashMap;
import java.util.Optional;

/**
 * Created by HorribleNerd on 11/12/2020
 */
public class NecklaceStorage implements INecklaceStorage {

    HashMap<Blessing, Integer> BLESSINGS;

    public NecklaceStorage() {
        BLESSINGS = new HashMap<>();
        BLESSINGS.put(Blessing.ATTACK, 0);
        BLESSINGS.put(Blessing.DEFENSE, 0);
        BLESSINGS.put(Blessing.MOVEMENT, 0);
        BLESSINGS.put(Blessing.SOUL, 0);
        BLESSINGS.put(Blessing.UTILITY, 0);
    }

    @Override
    public int getTier(Blessing blessingType) {
        return Optional.ofNullable(BLESSINGS.get(blessingType)).orElse(0);
    }

    @Override
    public void setTier(Blessing blessingType, int tier) {
        BLESSINGS.put(blessingType, tier);
    }

    public int getTierBonus() {
        return BLESSINGS.values().stream().reduce(0, Integer::sum);
    }

    @Override
    public void deserializeNBT(CompoundNBT tag) {
        for (String key : tag.keySet()) {
            Blessing b = Blessing.valueOf(key);
            BLESSINGS.put(b, tag.getInt(key));
        }
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT nbt = new CompoundNBT();
        BLESSINGS.forEach((blessing, i) -> nbt.putInt(blessing.name(), i));
        return nbt;
    }
}
