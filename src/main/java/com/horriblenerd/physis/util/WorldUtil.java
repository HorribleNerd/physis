package com.horriblenerd.physis.util;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Created by HorribleNerd on 04/12/2020
 */
public class WorldUtil {

    // Based on Mekanism
    public static boolean isBlockLoaded(@Nullable IBlockReader world, @Nonnull BlockPos pos) {
        if (world == null || !World.isValid(pos)) {
            return false;
        }
        else if (world instanceof IWorldReader) return ((IWorldReader) world).isBlockLoaded(pos);
        return true;
    }

    // Based on Mekanism
    public static <T extends TileEntity> T getTileEntity(@Nonnull Class<T> clazz, @Nullable IBlockReader world, @Nonnull BlockPos pos) {
        if (!isBlockLoaded(world, pos)) {
            return null;
        }
        TileEntity tile = world.getTileEntity(pos);
        if (tile == null) return null;
        if (clazz.isInstance(tile)) {
            return clazz.cast(tile);
        }
        return null;
    }

}
