package com.horriblenerd.physis.capability.soul;


import net.minecraft.nbt.CompoundNBT;

/**
 * Created by HorribleNerd on 04/12/2020
 */

public interface ISoulStorage {

    int receiveSouls(int maxReceive, boolean simulate);

    int extractSouls(int maxExtract, boolean simulate);

    void setSouls(int souls);

    void addSouls(int souls);

    void removeSouls(int souls);

    void onSoulsChanged();

    int getSoulsStored();

    int getMaxSoulsStored();

    int getMaxReceive();

    int getMaxExtract();

    boolean canExtract();

    boolean canReceive();

    CompoundNBT serializeNBT();

    void deserializeNBT(CompoundNBT tag);

}