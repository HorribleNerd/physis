package com.horriblenerd.physis.capability.necklace;

import net.minecraft.nbt.CompoundNBT;

/**
 * Created by HorribleNerd on 11/12/2020
 */
public interface INecklaceStorage {

    int getTier(Blessing blessingType);

    void setTier(Blessing blessingType, int tier);

    int getTierBonus();

    void deserializeNBT(CompoundNBT tag);

    CompoundNBT serializeNBT();

    enum Blessing {
        ATTACK,
        DEFENSE,
        MOVEMENT,
        SOUL,
        UTILITY;

        @Override
        public String toString() {
            return super.toString();
        }
    }

}
