package com.horriblenerd.physis.gui;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.horriblenerd.physis.PhysisConstants.MODID;

/**
 * Created by HorribleNerd on 10/12/2020
 */
@OnlyIn(Dist.CLIENT)
public class NecklaceButton extends Button {

    private static final Logger LOGGER = LogManager.getLogger();

    public NecklaceButton(int x, int y, int width, int height, ITextComponent title, IPressable pressedAction) {
        super(x, y, width, height, title, pressedAction);
    }

    public NecklaceButton(int x, int y, int width, int height, ITextComponent title, IPressable pressedAction, ITooltip onTooltip) {
        super(x, y, width, height, title, pressedAction, onTooltip);
    }

    @Override
    public void renderButton(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        Minecraft.getInstance().getTextureManager().bindTexture(Button.WIDGETS_LOCATION);
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        int x;
        int y;
        x = 0;
        y = 146;
        if (this.isHovered()) {
            x = 20;
            y = 166;
        }
        this.blit(matrixStack, this.x, this.y, x, y, this.width, this.height);

        if (this.isHovered()) {
            this.renderToolTip(matrixStack, mouseX, mouseY);
        }
    }

    public static void addInventoryButton(GuiScreenEvent.InitGuiEvent.Post event) {
        final Screen gui = event.getGui();
        if (gui instanceof ContainerScreen || gui.getClass().getName().contains("CuriosScreen")) {
            LOGGER.debug("Adding new necklace button");
            StringTextComponent title = new StringTextComponent("Necklace"); //I18n.format("physis.necklace_button")
            Button necklaceButton;

            int guiLeft = 177 + (gui.width - 176 - 200) / 2;

            necklaceButton = new NecklaceButton(guiLeft, gui.height / 2 + 100, 20, 20, title, (button) -> {
                LOGGER.debug("press");
            }, (button, matrixStack, mouseX, mouseY) -> {
                StringTextComponent tooltip = new StringTextComponent("Locked"); //I18n.format("physis.necklace_button")
                if (Minecraft.getInstance().player.isCreative()) { // If necklace unlocked
                    tooltip = new StringTextComponent("Necklace"); //I18n.format("physis.necklace_button")
                }
                gui.renderTooltip(matrixStack, tooltip, mouseX, mouseY);
            });

            event.addWidget(necklaceButton);
        }
    }

}
