package com.horriblenerd.physis.block.tile;

import com.horriblenerd.physis.capability.soul.CapabilitySouls;
import com.horriblenerd.physis.capability.soul.ISoulStorage;
import com.horriblenerd.physis.capability.soul.SoulStorage;
import com.horriblenerd.physis.data.WorldPlayerData;
import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import static com.horriblenerd.physis.PhysisConstants.getSoulCollectorAmount;
import static com.horriblenerd.physis.setup.Registration.SOUL_COLLECTOR_TILE;

/**
 * Created by HorribleNerd on 04/12/2020
 */
public class SoulCollectorTile extends TileEntity implements ITickableTileEntity {

    private static final Logger LOGGER = LogManager.getLogger();

    private SoulStorage soulStorage = createSoulStorage();
    private LazyOptional<ISoulStorage> souls = LazyOptional.of(() -> soulStorage);

    private UUID ownerUUID;
    private String ownerName;

    private int counter;

    public SoulCollectorTile() {
        super(SOUL_COLLECTOR_TILE.get());
    }

    @Override
    public void remove() {
        souls.invalidate();
    }

    public UUID getOwnerUUID() {
        return ownerUUID;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerUUID(UUID uuid) {
        ownerUUID = uuid;
    }

    public void setOwnerName(String name) {
        ownerName = name;
    }

    @Override
    public void tick() {
        if (world.isRemote) {
            return;
        }

        if (counter > 0) {
            counter--;

            if (counter <= 0) {
                if (this.world instanceof ServerWorld) {
                    ServerWorld serverWorld = (ServerWorld) this.world;
                    WorldPlayerData worldPlayerData = WorldPlayerData.get(serverWorld);
//                    LOGGER.debug(this::getOwnerName);
//                    LOGGER.debug(this::getOwnerUUID);
//                    LOGGER.debug("Standing: " + worldPlayerData.getPlayerStanding(getOwnerUUID()));
                    worldPlayerData.setPlayerStanding(getOwnerUUID(), 10);
                    soulStorage.addSouls(getSoulCollectorAmount(worldPlayerData.getPlayerStanding(getOwnerUUID())));
//                    LOGGER.debug("New souls: " + soulStorage.getSoulsStored());
                }
            }

            markDirty();
        }

        if (counter <= 0) {
            counter = 20;
            markDirty();
        }

        sendOutPower();
    }

    private void sendOutPower() {
        AtomicInteger capacity = new AtomicInteger(soulStorage.getSoulsStored());
        if (capacity.get() > 0) {
            for (Direction direction : Direction.values()) {
                TileEntity te = world.getTileEntity(pos.offset(direction));
                if (te != null) {
                    boolean doContinue = te.getCapability(CapabilitySouls.SOULS, direction).map(handler -> {
                                if (handler.canReceive()) {
                                    int received = handler.receiveSouls(Math.min(capacity.get(), 100), false);
                                    capacity.addAndGet(-received);
                                    soulStorage.extractSouls(received, false);
                                    markDirty();
                                    return capacity.get() > 0;
                                }
                                else {
                                    return true;
                                }
                            }
                    ).orElse(true);
                    if (!doContinue) {
                        return;
                    }
                }
            }
        }
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if (cap == CapabilitySouls.SOULS) {
            return souls.cast();
        }
        return super.getCapability(cap, side);
    }

    @Override
    public void read(BlockState state, CompoundNBT tag) {
        super.read(state, tag);
        soulStorage.deserializeNBT(tag.getCompound("souls"));
        ownerUUID = tag.getUniqueId("owner_uuid");
        ownerName = tag.getString("owner_name");
        counter = tag.getInt("counter");
    }

    @Override
    public CompoundNBT write(CompoundNBT tag) {
        super.write(tag);
        tag.put("souls", soulStorage.serializeNBT());
        tag.putUniqueId("owner_uuid", ownerUUID);
        tag.putString("owner_name", ownerName);
        tag.putInt("counter", counter);
        return tag;
    }

    private SoulStorage createSoulStorage() {
        return new SoulStorage(1000) {
            @Override
            public void onSoulsChanged() {
                markDirty();
            }
        };
    }

}
