package com.horriblenerd.physis.capability.owner;

import com.horriblenerd.physis.data.WorldPlayerData;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Util;
import net.minecraft.world.server.ServerWorld;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.UUID;

/**
 * Created by HorribleNerd on 04/12/2020
 */
public class OwnerStorage implements IOwnerStorage {

    protected UUID uuid;
    protected String name;

    public OwnerStorage(@Nonnull UUID uuid, @Nullable String name) {
        this.uuid = uuid;
        this.name = name != null ? name : "";
    }

    public OwnerStorage() {
        this.uuid = Util.DUMMY_UUID;
        this.name = "--Missing--";
    }

    @Override
    public UUID getOwnerUUID() {
        return this.uuid;
    }

    @Override
    public String getOwnerName() {
        return this.name;
    }

    @Override
    public void setOwnerUUID(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public void setOwnerName(String name) {
        this.name = name;
    }

    @Override
    public int getOwnerStanding(ServerWorld serverWorld) {
        return WorldPlayerData.get(serverWorld).getPlayerStanding(this.uuid);
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.putUniqueId("owner_uuid", this.uuid);
        tag.putString("owner_name", this.name);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT tag) {
        this.uuid = tag.getUniqueId("owner_uuid");
        this.name = tag.getString("owner_name");
    }
}
