package com.horriblenerd.physis.item;

import com.horriblenerd.physis.block.tile.BasicSoulTile;
import com.horriblenerd.physis.capability.owner.CapabilityOwner;
import com.horriblenerd.physis.capability.soul.CapabilitySouls;
import com.horriblenerd.physis.util.WorldUtil;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by HorribleNerd on 04/12/2020
 */
public class DebugItem extends ArmorItem {

    private static final Logger LOGGER = LogManager.getLogger();

    public DebugItem(IArmorMaterial materialIn, Properties builderIn) {
        super(materialIn, EquipmentSlotType.HEAD, builderIn);
    }

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
        if (!worldIn.isRemote()) {
            if (itemSlot == 3 && entityIn instanceof PlayerEntity) {
                PlayerEntity playerEntity = (PlayerEntity) entityIn;
                playerEntity.sendStatusMessage(new StringTextComponent(""), true);
                BlockRayTraceResult blockRayTraceResult = rayTrace(worldIn, playerEntity, RayTraceContext.FluidMode.NONE);
                BasicSoulTile tile = WorldUtil.getTileEntity(BasicSoulTile.class, worldIn, blockRayTraceResult.getPos());
                if (tile != null) {
                    AtomicReference<String> name = new AtomicReference<>("---");
                    AtomicReference<UUID> uuid = new AtomicReference<>(Util.DUMMY_UUID);
                    AtomicInteger souls = new AtomicInteger(-1);
                    AtomicInteger maxSouls = new AtomicInteger(-1);
                    AtomicInteger maxIn = new AtomicInteger(-1);
                    AtomicInteger maxOut = new AtomicInteger(-1);

                    tile.getCapability(CapabilityOwner.OWNER).ifPresent((ownerStorage -> {
                        name.set(ownerStorage.getOwnerName());
                        uuid.set(ownerStorage.getOwnerUUID());
                    }));

                    tile.getCapability(CapabilitySouls.SOULS).ifPresent((soulStorage -> {
                        souls.set(soulStorage.getSoulsStored());
                        maxSouls.set(soulStorage.getMaxSoulsStored());
                        maxIn.set(soulStorage.getMaxReceive());
                        maxOut.set(soulStorage.getMaxExtract());
                    }));

                    StringTextComponent debugString;
                    if (playerEntity.isSneaking()) {
                        debugString = new StringTextComponent(String.format("Souls: [%d/%d], mInOut: [%d/%d], Owner: %s, UUID: %s", souls.get(), maxSouls.get(), maxIn.get(), maxOut.get(), name.get(), uuid.get().toString()));
                    }
                    else {
                        debugString = new StringTextComponent(String.format("Souls: [%d/%d], Owner: %s", souls.get(), maxSouls.get(), name.get()));
                    }
//                    LOGGER.debug(String.format("Souls: [%d/%d], Owner: %s, UUID: %s", souls.get(), maxSouls.get(), name.get(), uuid.get().toString()));
                    playerEntity.sendStatusMessage(debugString, true);
                }
            }
        }
    }
}
