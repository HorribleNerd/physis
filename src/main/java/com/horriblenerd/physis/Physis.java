package com.horriblenerd.physis;

import com.horriblenerd.physis.setup.ClientSetup;
import com.horriblenerd.physis.setup.ModSetup;
import com.horriblenerd.physis.setup.Registration;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(PhysisConstants.MODID)
public class Physis {
    // Directly reference a log4j logger.
    private static final Logger LOGGER = LogManager.getLogger();

    public Physis() {

//        ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, Config.CLIENT_CONFIG);
//        ModLoadingContext.get().registerConfig(ModConfig.Type.SERVER, Config.SERVER_CONFIG);

        Registration.init();
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::preInit);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(ModSetup::init);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(ClientSetup::init);

        // Register the setup method for modloading
//        FMLJavaModLoadingContext.get().getModEventBus().addListener(ModSetup::init);
//        FMLJavaModLoadingContext.get().getModEventBus().addListener(ClientSetup::init);

    }

    public void preInit(FMLCommonSetupEvent event) {

    }

}
