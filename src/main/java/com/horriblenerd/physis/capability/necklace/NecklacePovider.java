package com.horriblenerd.physis.capability.necklace;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Created by HorribleNerd on 11/12/2020
 */
public class NecklacePovider implements ICapabilitySerializable<CompoundNBT> {

    private final NecklaceStorage necklaceStorage = new NecklaceStorage();
    private final LazyOptional<INecklaceStorage> necklaceOptional = LazyOptional.of(() -> necklaceStorage);

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        return necklaceOptional.cast();
    }

    @Override
    public CompoundNBT serializeNBT() {
        if (CapabilityNecklace.NECKLACE == null) {
            return new CompoundNBT();
        }
        else {
            return (CompoundNBT) CapabilityNecklace.NECKLACE.writeNBT(necklaceStorage, null);
        }
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        if (CapabilityNecklace.NECKLACE != null) {
            CapabilityNecklace.NECKLACE.readNBT(necklaceStorage, null, nbt);
        }
    }
}
