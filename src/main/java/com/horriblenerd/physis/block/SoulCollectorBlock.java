package com.horriblenerd.physis.block;

import com.horriblenerd.physis.block.tile.SoulCollectorTile;
import com.horriblenerd.physis.data.WorldPlayerData;
import com.horriblenerd.physis.util.WorldUtil;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;

import javax.annotation.Nullable;
import java.lang.ref.WeakReference;
import java.util.UUID;

/**
 * Created by HorribleNerd on 04/12/2020
 */
public class SoulCollectorBlock extends BasicSoulBlock {

    public SoulCollectorBlock(Properties prop) {
        super(prop);
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack stack) {
        SoulCollectorTile tile = WorldUtil.getTileEntity(SoulCollectorTile.class, worldIn, pos);
        if (tile != null) {
            if (!worldIn.isRemote()) {
                if (placer instanceof ServerPlayerEntity) {
                    ServerPlayerEntity player = (ServerPlayerEntity) placer;
                    tile.setOwnerUUID(player.getUniqueID());
                    tile.setOwnerName(player.getName().getString());

                    WorldPlayerData.get((ServerWorld) worldIn).subtractPlayerStanding(player.getUniqueID(), 10);
                }
            }
        }
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return true;
    }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return new SoulCollectorTile();
    }
}
