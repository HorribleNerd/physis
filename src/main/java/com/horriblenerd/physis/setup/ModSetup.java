package com.horriblenerd.physis.setup;

import com.horriblenerd.physis.capability.necklace.CapabilityNecklace;
import com.horriblenerd.physis.capability.owner.CapabilityOwner;
import com.horriblenerd.physis.capability.soul.CapabilitySouls;
import com.horriblenerd.physis.handlers.NecklaceEventHandlers;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.horriblenerd.physis.PhysisConstants.MODID;

/**
 * Created by HorribleNerd on 04/12/2020
 */
@Mod.EventBusSubscriber(modid = MODID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class ModSetup {

    private static final Logger LOGGER = LogManager.getLogger();

    public static final ItemGroup ITEM_GROUP = new ItemGroup(MODID) {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(Blocks.BIRCH_SAPLING);
        }
    };

    public static void init(final FMLCommonSetupEvent event) {
//        Networking.registerMessages();
//        CapabilityEntityCharge.register();
//
        CapabilitySouls.register();
        CapabilityOwner.register();
        CapabilityNecklace.register();

        MinecraftForge.EVENT_BUS.addGenericListener(Entity.class, NecklaceEventHandlers::attachCapabilities);
        MinecraftForge.EVENT_BUS.addListener(NecklaceEventHandlers::onDamage);
        MinecraftForge.EVENT_BUS.addListener(NecklaceEventHandlers::onAttack);
        MinecraftForge.EVENT_BUS.addListener(NecklaceEventHandlers::onBlockBreak);

//        MinecraftForge.EVENT_BUS.addListener(ChargeEventHandler::onAttachCapabilitiesEvent);
//        MinecraftForge.EVENT_BUS.addListener(ChargeEventHandler::onAttackEvent);
//        MinecraftForge.EVENT_BUS.addListener(ChargeEventHandler::onDeathEvent);
    }

    @SubscribeEvent
    public static void serverLoad(FMLServerStartingEvent event) {
//        ModCommands.register(event.getCommandDispatcher());
    }

//    @SubscribeEvent
//    public static void onDimensionRegistry(RegisterDimensionsEvent event) {
//        ModDimensions.DIMENSION_TYPE = DimensionManager.registerOrGetDimension(ModDimensions.DIMENSION_ID, Registration.DIMENSION.get(), null, true);
//    }

    /*
    @OnlyIn(Dist.CLIENT)
    @SubscribeEvent
    public static void addDebugText(RenderGameOverlayEvent.Text event) {
        if (event.getType() == RenderGameOverlayEvent.ElementType.TEXT) {

            RayTraceResult mouseOver = Minecraft.getInstance().objectMouseOver;
            if (mouseOver != null) {
                if (mouseOver.getType() == RayTraceResult.Type.BLOCK) {
                    ClientPlayerEntity player = Minecraft.getInstance().player;
                    BlockPos pos = ((BlockRayTraceResult) mouseOver).getPos();

                    BasicSoulTile tile = WorldUtil.getTileEntity(BasicSoulTile.class, player.getEntityWorld(), pos);

                    ArrayList<String> debug = new ArrayList<>();
                    debug.add("");

                    if (tile != null) {
                        debug.add("Physis info");
                        debug.add("==========");
                        tile.getCapability(CapabilityOwner.OWNER).ifPresent((ownerStorage -> {
                            debug.add(String.format("Owner: %s", ownerStorage.getOwnerName()));
                            debug.add(String.format("Owner UUID: %s", ownerStorage.getOwnerUUID()));
                        }));

                        tile.getCapability(CapabilitySouls.SOULS).ifPresent((soulStorage -> {
                            debug.add(String.format("Souls stored: [%d/%d]", soulStorage.getSoulsStored(), soulStorage.getMaxSoulsStored()));
                        }));
                    }

//                    LOGGER.debug(event.getLeft().toString());
//                    LOGGER.debug(event.getRight().toString());

                    event.getRight().addAll(debug);
//                    event.getLeft().add(debug.toString());
                }
            }

        }
    }
     */

}